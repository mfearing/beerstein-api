package com.mjf.beerstein.entities.projections;

import org.springframework.data.rest.core.config.Projection;

import com.mjf.beerstein.entities.User;

@Projection(name="partial", types = {User.class})
public interface PartialUserProjection {
	String getResourceId();
	String getName();
}
