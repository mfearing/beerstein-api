package com.mjf.beerstein.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.rest.core.annotation.RestResource;

@Entity
public class Recipe extends AbstractEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
    
    @ManyToOne(fetch = FetchType.EAGER)
	@RestResource(exported=false)
    private Category category;

    @OneToMany(mappedBy="recipe", fetch=FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true)
    private Set<Ingredient> ingredients;
    
    @Column
	private String directions;

	public User getUser() {
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }
    
    public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	//Use this to grab id
	public Long getResourceId() {
		return id;
    }
    
    public Set<Ingredient> getIngredients(){
        return ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients){
        this.ingredients = ingredients;
    }

    public String getDirections() {
		return directions;
	}

	public void setDirections(String directions) {
		this.directions = directions;
	}

}