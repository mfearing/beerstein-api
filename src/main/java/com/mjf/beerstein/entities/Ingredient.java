package com.mjf.beerstein.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Ingredient extends AbstractEntity {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "ID", nullable = false, updatable = false)
	private Recipe recipe;

	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;

	@Column(nullable = false)
	private Double quantity;

	@Column
	private String measurement;

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getMeasurement() {
		return measurement;
	}

	public void setMeasurement(String measurement) {
		this.measurement = measurement;
	}

	public Long getResourceId() {
		return id;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
}