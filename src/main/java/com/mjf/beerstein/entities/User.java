package com.mjf.beerstein.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class User extends AbstractEntity {

	@Column(nullable = false)
	String pass;

	public String getpass() {
		return pass;
	}

	public void setpass(String pass) {
		this.pass = pass;
	}

	public Long getResourceId() {
		return id;
	}
}