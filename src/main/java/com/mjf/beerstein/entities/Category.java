package com.mjf.beerstein.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Category extends AbstractEntity {

	@Column(nullable = false)
	String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Long getResourceId() {
		return id;
	}
}
