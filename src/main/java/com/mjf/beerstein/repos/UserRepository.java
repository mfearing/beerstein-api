package com.mjf.beerstein.repos;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mjf.beerstein.entities.User;
import com.mjf.beerstein.entities.projections.PartialUserProjection;

@RepositoryRestResource(excerptProjection=PartialUserProjection.class)
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	Page<User> findByName(@Param("name") String name, Pageable pageable);
}
