package com.mjf.beerstein.repos;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.mjf.beerstein.entities.Recipe;

public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long> {
    
	Page<Recipe> findByName(@Param("name") String name, Pageable pageable);

	Page<Recipe> findByUserId(@Param("id") Long id, Pageable pageable);
	
}