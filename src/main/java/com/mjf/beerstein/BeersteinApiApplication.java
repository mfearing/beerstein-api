package com.mjf.beerstein;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeersteinApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeersteinApiApplication.class, args);
	}
}
