package com.mjf.beerstein.repos;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.mjf.beerstein.entities.Ingredient;

public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long> {

	Page<Ingredient> findByName(@Param("name") String name, Pageable pageable);
	
	Page<Ingredient> findByRecipeId(@Param("id") Long id, Pageable pageable);
}
